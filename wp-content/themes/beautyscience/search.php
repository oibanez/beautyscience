<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>


	<div class="container-fluid">
		<div class="row">
			<div class="container">
				<div class="page-section">
					<div class="col-sm-12">

					<?php if ( have_posts() ) : ?>
					<header class="page-header-search">
						<h3 class="page-heading"><?php printf( __( 'Search Results for: %s', 'twentyfourteen' ), get_search_query() ); ?></h3>
					</header><!-- .page-header -->
					<ul class="search-list">
						<?php 
							// Start the Loop.
							while ( have_posts() ) : the_post();

								/*
								 * Include the post format-specific template for the content. If you want to
								 * use this in a child theme, then include a file called called content-___.php
								 * (where ___ is the post format) and that will be used instead.
								 */
								?>
								<li>
								<div class="entry-meta">
									<?php
										if ( 'post' == get_post_type() )
											twentyfourteen_posted_on();

										if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
									?>
									<!-- <span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'twentyfourteen' ), __( '1 Comment', 'twentyfourteen' ), __( '% Comments', 'twentyfourteen' ) ); ?></span> -->
									<?php
										endif;

										edit_post_link( __( 'Edit', 'twentyfourteen' ), '<span class="edit-link">', '</span>' );
									?>
								</div><!-- .entry-meta -->
								<a href="<?php the_permalink(); ?>" class="permalink">
									<h4><?php the_title(); ?></h4>
						   			<!-- <div class="entry-meta">
										<span class="cat-links">Category: <?php echo get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'twentyfourteen' ) ); ?></span>
									</div> -->

									<span class="blog-excerpt">
										<?php the_excerpt(); ?>
									</span>

								</a>
								</li>

								<?php 

							endwhile;
							// Previous/next post navigation.

						else :
							// If no content, include the "No posts found" template.
							get_template_part( 'content', 'none' );

						endif;
					?>
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>





<?php
get_footer();
