<?php

/*

	Template Name: Home Template

 */

get_header(); ?>
<style type="text/css">
	ul#menu-main-menu > li:first-child > a {
		color: #00958e;
	}
</style>
<div class="container-fluid">
	<div class="row">
		<!-- <div class="container carousel-con-hp"> -->
			<div id="carousel-hp" class="carousel slide" data-ride="carousel">
				  

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
				    <div class="item active">
				      	<a href="#"><img src="<?php echo bloginfo('template_directory');?>/img/banners/1.jpg" style="width: 100%;"></a>
				    </div>
				    <div class="item">
				      	<a href="#"><img src="<?php echo bloginfo('template_directory');?>/img/banners/2.jpg" style="width: 100%;"></a>
				    </div>
				    <div class="item">
				      	<a href="#"><img src="<?php echo bloginfo('template_directory');?>/img/banners/3.jpg" style="width: 100%;"></a>
				    </div>
				    <div class="item">
				      	<a href="#"><img src="<?php echo bloginfo('template_directory');?>/img/banners/4.jpg" style="width: 100%;"></a>
				    </div>
				    <div class="item">
				      	<a href="#"><img src="<?php echo bloginfo('template_directory');?>/img/banners/5.jpg" style="width: 100%;"></a>
				    </div>
				  </div>

				  <!-- Controls -->
				  <!-- <a class="left carousel-control" href="#carousel-hp" role="button" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#carousel-hp" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a> -->

				  <ol class="carousel-indicators">
				    <li data-target="#carousel-hp" data-slide-to="0" class="active"></li>
				    <li data-target="#carousel-hp" data-slide-to="1"></li>
				    <li data-target="#carousel-hp" data-slide-to="2"></li>
				    <li data-target="#carousel-hp" data-slide-to="3"></li>
				    <li data-target="#carousel-hp" data-slide-to="4"></li>
				  </ol>
			</div>
		<!-- </div> -->
	</div>
</div>

<!-- CALLOUTS -->
<div class="container-fluid">
	<div class="row">
		<div class="callouts-container">
			<ul class="callouts">
				<li>
					<a href="<?php echo get_permalink('1719');?>">
						<div class="img-container">
							<div class="green-overlay"></div>
							<img src="<?php echo bloginfo('template_directory');?>/img/callouts/1.png">
							<p>Brow/Lash<br>Bar</p>
						</div>
						<h3>Brow/Lash Bar</h3>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="img-container">
							<div class="green-overlay"></div>
							<img src="<?php echo bloginfo('template_directory');?>/img/callouts/2.png">
							<p>Shop<br>Now</p>
						</div>
						<h3>Online Store</h3>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="img-container">
							<div class="green-overlay"></div>
							<img src="<?php echo bloginfo('template_directory');?>/img/callouts/3.png">
							<p>Book<br>Now</p>
						</div>
						<h3>Web Booking</h3>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="img-container">
							<div class="green-overlay"></div>
							<img src="<?php echo bloginfo('template_directory');?>/img/callouts/4.png">
							<p>Gift<br>Cards</p>
						</div>
						<h3>Gift Cards</h3>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="container-fluid footer-cont-rp reset-padding">
	<div class="row reset-margin">
		<div class="container">
			<footer class="footer-container">
				<p class="text-center footer-text">
					Established in 1994, Beauty Science was the first 'beauty salon' to operate out of a busy shopping centre. With almost 20 years experience in the industry, we understood our client's need for convenience, efficiency, quality and access to innovative treatments and products. Our new concept "Express Bar" is ideal for busy working women and mums. Whether you require hair removal, skin treatments, brow styling, lashes or makeup, the "Express Bar" provides quick treatments available at anytime without the need to make an appointment.
				</p>
				<?php echo get_template_part('footer-callouts');?>
			</footer>
		</div>
	</div>
	<div class="row reset-margin">
		<p class="copyright">&copy; Beauty Science <?php echo date('Y');?> All Rights Reserved.<br>
			<a href="http://spafresh.com.au">A Spa Fresh Design</a>
		</p>
	</div>
</div>



<button type="button" class="btn btn-primary btn-lg hidden" data-toggle="modal" data-target="#myModal">
  Launch
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" style="background: none; box-shadow: none; border: none; margin-top: 50px;">
      <div class="modal-body reset-padding">
        <button type="button" class="close close-popup-mc" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

<div data-role="popup" data-theme="e" data-overlay-theme="a" data-history="false" id="popupBasic" style="margin-top: 20px;">
  <div id="mainpop_up">
    <div class="popupheader">
      <img src="<?php echo bloginfo('template_directory');?>/img/popuplogo.png" class="img-responsive" border="0" alt="Beauty Science" style="float:left;">
      <h1>Become a Beauty Science VIP</h1>
    </div>
    <div class="popupbody">
      <div class="pop_left">
        Fill in the form and<br />
        receive our exciting<br />
        new members special<br />
        offers via email.
        <div class="clear">
        </div>
        <br />
        <span class="note">*not available on gift vouchers or stock</span>
      </div>
      <div class="pop_right">
        <script>
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};

function senddata() {
	var formid = 'mc-embedded-subscribe-form';
	var error = '';
	$("#mc-embedded-subscribe-form .error_value").each(function( index ) {
		$(this).removeClass('error_value');
	});
	
	 if(!isValidEmailAddress($('#mce-EMAIL').val()) ) {
		error += 'Invalid Email Address\n';
		$('#mce-EMAIL').addClass('error_value');
	 }
	 if($('#mce-FNAME').val() == '' ) {
		 $('#mce-FNAME').addClass('error_value');
		error += 'First Name is Required\n';
	}
	if($('#mce-LNAME').val() == '' ) {
		$('#mce-LNAME').addClass('error_value');
		error += 'Last Name is Required\n';
	}
	
	if(error != '') {
		alert(error);
		
	}else {
			
		$('#mc-embedded-subscribe-form').submit( function () {
		//alert('test');  
		            $.ajax({   
                            type: "POST",
                            data : $(this).serialize(),
                            cache: false,  
                            url: $(this).attr('action')
							
                        }); 
		//$("#popupBasic").popup("close");	 
			 alert('Almost finished...\n\nWe need to confirm your email address.\nTo complete the subscription process, please click the link in the email we just sent you.');
			 $('#mce-EMAIL').val('');
			 $('#mce-FNAME').val('');
			 $('#mce-LNAME').val('');
					
		     
            });   

	}
	
	return false;	
}
</script>
        <form onSubmit="return senddata();" action="http://beautyscience.us8.list-manage2.com/subscribe/post?u=ed673ca0e535acd953f776814&amp;id=4360ed8434" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
          <div class="mc-field-group">
            <label for="mce-EMAIL">Email Address <span class="asterisk">*</span>&nbsp;</label>
            <div class="clear">
            </div>
            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" style="-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
          </div>
          <div class="mc-field-group">
            <label for="mce-FNAME">First Name <span class="asterisk">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
            <div class="clear">
            </div>
            <input type="text" value="" name="FNAME" class="" id="mce-FNAME" style="-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
          </div>
          <div class="mc-field-group">
            <label for="mce-LNAME">Last Name <span class="asterisk">*</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
            <div class="clear">
            </div>
            <input type="text" value="" name="LNAME" class="" id="mce-LNAME" style="-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
          </div>
          
          
          <div class="mc-field-group">
            
               <input type="radio" name="MMERGE3" id="mce-MMERGE3" value="Chadstone" style="width:15px;height:15px;margin:0 0 0;" checked>&nbsp;&nbsp;Chadstone&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" style="width:15px;height:15px;margin:0 0 0;" name="MMERGE3" id="mce-MMERGE33" value="Richmond">&nbsp;&nbsp;Richmond 
           
            <div class="clear">
            </div>
          </div>
          
          
          <div id="mce-responses" class="clear">
            <div class="response" id="mce-error-response" style="display:none">
            </div>
            <div class="response" id="mce-success-response" style="display:none">
            </div>
          </div>
          <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
          <div style="position: absolute; left: -5000px; float:right;">
            <input type="text" name="b_ed673ca0e535acd953f776814_4360ed8434" value="">
          </div>
          <div class="clear" >
            <input type="submit" name="subscribe" id="mc-embedded-subscribe" class="button submit">
          </div>
        </form>
      </div>
      <div class="clear">
      </div>
    </div>
  </div>
</div>

      </div>

    </div>
  </div>
</div>

<script type="text/javascript">
		$(document).ready(function() {
			/*
			*   Examples - images
			*/
			//$.cookie("testCookie", "hello");
			//alert($.cookie("testCookie"));
			var date = new Date();
 			//var minutes = 1;
			var minutes = 1440;
			date.setTime(date.getTime() + (minutes * 60 * 1000));
			//date.setTime(date.getTime() + (minutes * 2));
			if(jQuery.cookie('jpopup') != 1){
			   //jQuery.cookie("jpopup", 1, { expires : 10 });
			   jQuery.cookie("jpopup", 1, { expires : date });
			   // jQuery("a#various3").trigger('click');
			   $('#myModal').modal('show');
			}

		});
</script>




<?php get_footer('index');?>

