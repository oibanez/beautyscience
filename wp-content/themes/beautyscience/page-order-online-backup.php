<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
get_header();
?>


<div class="container-fluid" style="color: #000">
    <div class="row">
        <div class="inner-banner-container">
<?php the_post_thumbnail('full'); ?>
            <h3 class="page-heading"><?php the_title(); ?></h3>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <div class="page-section">
                <ul class="products">
                    <?php
                    $args = array(
                        'post_type' => 'product',
                        'posts_per_page' => 8
                    );
                    $loop = new WP_Query($args);
                    if ($loop->have_posts()) {
                        while ($loop->have_posts()) : $loop->the_post();
                            wc_get_template_part('content', 'product');
                        endwhile;
                    } else {
                        echo __('No products found');
                    }
                    wp_reset_postdata();
                    ?>
                </ul><!--/.products-->

            </div>
        </div>
    </div>
</div>
<?php
$object = new WC_Cart();
$array= $object->get_cart_from_session();

print_r($array);
?>




<?php get_footer(); ?>

