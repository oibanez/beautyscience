<?php
/*

	Template Name: Cellulite Template

 */
get_header('inner'); ?>


<?php /* The loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
<div class="container-fluid">
	<div class="row">
		<div class="container">
			<img src="<?php echo bloginfo('template_directory');?>/img/inner-banner.jpg" class="img-responsive center">
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="container-fluid inner-content">
		<div class="row">
			<div class="container inner-container">
				<div class="col-sm-8 main-content-column">
					<div class="green-inner-title">
						<h2 class="page-heading"><?php the_title(); ?></h2>
					</div>
					<div class="page-section">
						

						<h3>Venus Concept - Thigh Module 8</h3>
						<p>Per treatment (per area) $175<br>
						25% discount for package x5 treatments</p>
						<p><iframe width="100%" height="315" src="https://www.youtube.com/embed/-RwdpMajUj0" frameborder="0" allowfullscreen></iframe></p>
						<p>The Venus Freeze is a Magnetic Pulse treatment which reduces fat cells, wrinkles, cellulite and tightens loose skin with the use of magnetic fields and radiofrequency. </p>
						<p>Some of the reasons for considering the Venus Freeze include:</p>
						<ul>
							<li>improving the look, feel and shape of stubborn and resistant areas that do not respond to traditional exercise and diets</li>
							<li>decreases wrinkles and giving a fresh youthful appearance without the use of needles or surgery</li>
							<li>improving your appearance by tightening skin, circumferential reduction and decreasing cellulite</li>
							<li>desiring a non-invasive and painless approach</li>
							<li>ability to reach smaller and hard to reach areas</li>
						</ul>
						<br>
						<h4 class="hl-lt-green">About Magnetic Pulse Treatment (Venus Freeze)</h4>
						<?php the_content(); ?>

						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						  <!-- Wrapper for slides -->
						  <div class="carousel-inner" role="listbox">
						    <div class="item active">
						      <img src="<?php echo bloginfo('template_directory');?>/img/inner-carousel/cellulite/BA1.jpg" alt="" class="center img-responsive">
						    </div>
						    <div class="item">
						      <img src="<?php echo bloginfo('template_directory');?>/img/inner-carousel/cellulite/BA2.jpg" alt="" class="center img-responsive">
						    </div>
						    <div class="item">
						      <img src="<?php echo bloginfo('template_directory');?>/img/inner-carousel/cellulite/BA3.jpg" alt="" class="center img-responsive">
						    </div>
						    <div class="item">
						      <img src="<?php echo bloginfo('template_directory');?>/img/inner-carousel/cellulite/BA4.jpg" alt="" class="center img-responsive">
						    </div>
						    <div class="item">
						      <img src="<?php echo bloginfo('template_directory');?>/img/inner-carousel/cellulite/BA5.jpg" alt="" class="center img-responsive">
						    </div>
						    <div class="item">
						      <img src="<?php echo bloginfo('template_directory');?>/img/inner-carousel/cellulite/BA6.jpg" alt="" class="center img-responsive">
						    </div>
						    <div class="item">
						      <img src="<?php echo bloginfo('template_directory');?>/img/inner-carousel/cellulite/BA7.jpg" alt="" class="center img-responsive">
						    </div>
						    <div class="item">
						      <img src="<?php echo bloginfo('template_directory');?>/img/inner-carousel/cellulite/BA8.jpg" alt="" class="center img-responsive">
						    </div>
						    <div class="item">
						      <img src="<?php echo bloginfo('template_directory');?>/img/inner-carousel/cellulite/BA9.jpg" alt="" class="center img-responsive">
						    </div>
						  </div>
						 </div>	
						 <p>&nbsp;</p>
						<p class="text-center">
							<a href="<?php echo bloginfo('template_directory');?>/pdf/Venus Swan Brochure.pdf" target="_blank">
								<img src="<?php echo bloginfo('template_directory');?>/img/download-btn.png" class="img-responsive center">
							</a>
						</p>


						

					</div>
				</div>
				<div class="clearfix visible-xs"></div>
				<div class="col-sm-4 sidebar-column">
					<?php get_sidebar('inner');?>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-12">
					<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer>
				</div>
						

			</div>
		</div>
	</div>
<?php endwhile; ?>
<script type="text/javascript">
	$(function(){
		$('.carousel').carousel({
		  interval: 3000
		})
	});
</script>



<?php get_footer(); ?>

