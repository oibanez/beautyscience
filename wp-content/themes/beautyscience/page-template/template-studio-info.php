<?php
/*

	Template Name: Studio Info Template

 */
get_header('inner'); ?>


<?php /* The loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
<div class="container-fluid">
	<div class="row">
		<div class="container">
			<img src="<?php echo bloginfo('template_directory');?>/img/inner-banner.jpg" class="img-responsive center">
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="container-fluid inner-content">
		<div class="row">
			<div class="container inner-container">
				<div class="col-sm-8 main-content-column">
					<div class="green-inner-title">
						<h2 class="page-heading"><?php the_title(); ?></h2>
					</div>
					<div class="page-section">
                                           <?php the_content(); ?>
                                            <br/>
                                            <h3>ASSOCIATE BRANDS</h3>
                                           <div class="col-xs-12">
                                               <div class="col-md-5 rounded-box">
                                                   <img src="<?php echo bloginfo('template_directory');?>/img/medik8.jpg" class="img-responsive center">
                                               </div>
                                               <div class="col-md-5 rounded-box">
                                                   <img src="<?php echo bloginfo('template_directory');?>/img/repechage.jpg" class="img-responsive center">
                                               </div>
                                               <div class="col-md-5 rounded-box">
                                                   <img src="<?php echo bloginfo('template_directory');?>/img/venusfreeze.jpg" class="img-responsive center">
                                               </div>
                                               <div class="col-md-5 rounded-box">
                                                   <img src="<?php echo bloginfo('template_directory');?>/img/anastasia.jpg" class="img-responsive center">
                                               </div>
                                               <div class="col-md-5 rounded-box">
                                                   <img src="<?php echo bloginfo('template_directory');?>/img/brightwhitesmile.jpg" class="img-responsive center">
                                               </div>
                                               <div class="col-md-5 rounded-box">
                                                   <img src="<?php echo bloginfo('template_directory');?>/img/tuscantan.jpg" class="img-responsive center">
                                               </div>
                                               <div class="col-md-5 rounded-box">
                                                   <img src="<?php echo bloginfo('template_directory');?>/img/brows.jpg" class="img-responsive center">
                                               </div>
                                           </div>
					</div>
				</div>
				<div class="clearfix visible-xs"></div>
				<div class="col-sm-4 sidebar-column">
					<?php get_sidebar('inner');?>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-12">
					<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer>
				</div>
						

			</div>
		</div>
	</div>
<?php endwhile; ?>




<?php get_footer(); ?>

