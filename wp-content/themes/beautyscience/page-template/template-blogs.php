<?php
/*

	Template Name: Blogs Template

 */
get_header(); ?>


<div class="container-fluid">
		<div class="row">
			<div class="inner-banner-container">
				<?php the_post_thumbnail('full'); ?>
				<h3 class="page-heading"><?php the_title(); ?></h3>
			</div>
		</div>
		<div class="row">
			<div class="container">
				<div class="page-section">
					<div class="col-sm-9">
						<ul class="blog-posts">
								<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
								$args = array( 'post_type' => 'post', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 2, 'paged' => $paged, 'category_name' => 'blog' );
								$wp_query = new WP_Query($args);
								while ( have_posts() ) : the_post(); ?>
							<!-- --><li class="blog-list">
							   			
										<!-- <a href="<?php the_permalink(); ?>" class="permalink"> -->
											
											<!-- <div class="entry-meta">
												<span class="cat-links">Category: <?php echo get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'twentyfourteen' ) ); ?></span>
											</div>  -->
									
										<!-- </a> -->
												
												<h4 class="post-title"><?php the_title(); ?></h4>

												<div class="entry-meta">
													<?php
														if ( 'post' == get_post_type() )
															twentyfourteen_posted_on();?> IN
														<?php echo get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'twentyfourteen' ) );


														if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
													?>
													<!-- <span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'twentyfourteen' ), __( '1 Comment', 'twentyfourteen' ), __( '% Comments', 'twentyfourteen' ) ); ?></span> -->
													<?php
														endif;

														//edit_post_link( __( 'Edit', 'twentyfourteen' ), '<span class="edit-link">', '</span>' );
													?>
												</div><!-- .entry-meta -->

												<div><?php the_excerpt(); ?> </div>
												<div class="feat-img-container"><?php the_post_thumbnail(); ?></div>
												
												<div class="continue-reading-container">
													<?php
													if(function_exists('display_social4i'))
														echo display_social4i("large","float-left");
													?>
													<a href="<?php the_permalink(); ?>" class="continue-reading">CONTINUE READING &nbsp;<i class="fa fa-angle-right"></i></a>
												</div>
										
							   		</li><!-- -->
								<?php endwhile; ?>
							</ul>

							<!-- then the pagination links -->
							<div class="prev-next-container">
								<?php next_posts_link( '<span class="pull-left post-prev-blog">Previous</span>', $wp_query ->max_num_pages); ?>
								<?php previous_posts_link( '<span class="pull-right post-next-blog">Next</span>' ); ?>
							
								<?php //wpbeginner_numeric_posts_nav(); ?>

							</div>

					</div>
					<div class="clearfix visible-xs"></div>
					<div class="col-sm-3 sidebar">
						<?php get_sidebar('blogs'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>

