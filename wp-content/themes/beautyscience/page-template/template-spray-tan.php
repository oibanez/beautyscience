<?php
/*

	Template Name: Spray Tan Template

 */
get_header('inner'); ?>


<?php /* The loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
<div class="container-fluid">
	<div class="row">
		<div class="container">
			<img src="<?php echo bloginfo('template_directory');?>/img/inner-banner.jpg" class="img-responsive center">
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="container-fluid inner-content">
		<div class="row">
			<div class="container inner-container">
				<div class="col-sm-8 main-content-column">
					<div class="green-inner-title">
						<h2 class="page-heading"><?php the_title(); ?></h2>
					</div>
					<div class="page-section">
						<?php the_content(); ?>	

						<div class="inner-box ">
                                                        <div class="col-md-9">
                                                            <h3>Express sunless tanning (no appointment necessary)</h3>
                                                            <p>
                                                                <ul style="list-style-type: circle">
                                                                    <li>Flawless even tan in less than 2 mins</li>
                                                                    <li>Privacy - no need to stand naked in front of an operator</li>
                                                                    <li>Completely hygienic </li>
                                                                    <li>Heat Lamp for added comfort</li>
                                                                </ul>
                                                            </p>
                                                        </div>
                                                        <div class="col-md-3 content-price">
                                                            $40                                                                                                                  
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <img src="<?php echo bloginfo('template_directory');?>/img/tuscanpic.png" class="img-responsive center vcenter col-md-8">
                                                            <img src="<?php echo bloginfo('template_directory');?>/img/versaspa.png" class="img-responsive center col-md-4">
                                                        </div>
                                                </div>
					</div>
				</div>
				<div class="clearfix visible-xs"></div>
				<div class="col-sm-4 sidebar-column">
					<?php get_sidebar('inner');?>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-12">
					<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer>
				</div>
						

			</div>
		</div>
	</div>
<?php endwhile; ?>




<?php get_footer(); ?>

