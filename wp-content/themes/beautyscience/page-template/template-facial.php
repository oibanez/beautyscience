<?php
/*

	Template Name: Facial Rejuvenation Template

 */
get_header('inner'); ?>


<?php /* The loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
<div class="container-fluid">
	<div class="row">
		<div class="container">
			<img src="<?php echo bloginfo('template_directory');?>/img/inner-banner.jpg" class="img-responsive center">
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="container-fluid inner-content">
		<div class="row">
			<div class="container inner-container">
				<div class="col-sm-8 main-content-column">
					<div class="green-inner-title">
						<h2 class="page-heading"><?php the_title(); ?></h2>
					</div>
					<div class="page-section">
						<?php the_content(); ?>	

						<div class="inner-box">
							<h3>Skin Needling (Collagen Induction Therapy)</h3>
							<p>Per session $349<br>
							25% discount for package x3 treatments</p>
							<p>Designed to trigger new collagen synthesis in the skin triggering the skin healing mechanisms. The only treatment on the market that addresses fine lines, large pores, skin slackening, scarring and pigmentation all in one! Clinically proven results. Approx. x3-6 monthly treatments for long-term visible results! </p>

							<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
							  <!-- Wrapper for slides -->
							  <div class="carousel-inner" role="listbox">
							    <div class="item active">
							      <img src="<?php echo bloginfo('template_directory');?>/img/inner-carousel/facial-reju/BA10.jpg" alt="" class="center img-responsive">
							    </div>
							    <div class="item">
							      <img src="<?php echo bloginfo('template_directory');?>/img/inner-carousel/facial-reju/BA11.jpg" alt="" class="center img-responsive">
							    </div>
							    <div class="item">
							      <img src="<?php echo bloginfo('template_directory');?>/img/inner-carousel/facial-reju/BA12.jpg" alt="" class="center img-responsive">
							    </div>
							    <div class="item">
							      <img src="<?php echo bloginfo('template_directory');?>/img/inner-carousel/facial-reju/BA13.jpg" alt="" class="center img-responsive">
							    </div>
							    <div class="item">
							      <img src="<?php echo bloginfo('template_directory');?>/img/inner-carousel/facial-reju/BA14.jpg" alt="" class="center img-responsive">
							    </div>
							    <div class="item">
							      <img src="<?php echo bloginfo('template_directory');?>/img/inner-carousel/facial-reju/BA15.jpg" alt="" class="center img-responsive">
							    </div>
							    <div class="item">
							      <img src="<?php echo bloginfo('template_directory');?>/img/inner-carousel/facial-reju/BA16.jpg" alt="" class="center img-responsive">
							    </div>
							    <div class="item">
							      <img src="<?php echo bloginfo('template_directory');?>/img/inner-carousel/facial-reju/BA17.jpg" alt="" class="center img-responsive">
							    </div>
							    <div class="item">
							      <img src="<?php echo bloginfo('template_directory');?>/img/inner-carousel/facial-reju/BA18.jpg" alt="" class="center img-responsive">
							    </div>

							  </div>


							</div>
						</div>

						<div class="inner-box">
							<div class="col-md-8 reset-padding">
								<h3>Photo Dynamic Therapy</h3>
								<p>A lightweight 20% strength serum containing levunic acid and bergamot and used in combination with LED light Therapy produces greatly enhanced results for reducing acne, sun damaged tissue and hyperpigmentation without minimal if any downtime. Visible from the very first treatment! </p>
								<p><strong>Single session with LED Light Therapy</strong></p>
							</div>
							<div class="col-md-4 content-price"> $150 </div>
						</div>
						<div class="inner-box">
							<div class="col-md-8 reset-padding">
								<h3>Skin Tightening for Face & Neck</h3>
								<p>Per session Face & Neck $225<br>
								25% discount for package x4 treatments</p>
								<p>Packages <strong>(Per Session)</strong></p>
							</div>
							<div class="col-md-4 content-price from"><span>From</span><br>$150 </div>
							<div class="clearfix"></div>
							<p><iframe height="315" src="https://www.youtube.com/embed/cTNfjUHtk4g" frameborder="0" allowfullscreen style="width: 100%;"></iframe></p>
							<div class="clearfix"></div>
							<div class="col-md-8 reset-padding">
								<p>For more details on Venus Swan, Please see our PDF Brochure below :</p>
							</div>
							<div class="col-sm-4 text-right">
								<a href="<?php echo bloginfo('template_directory');?>/pdf/Venus_Swan_Patient_Brochure_Template.pdf" target="_blank"><img src="<?php echo bloginfo('template_directory');?>/img/pdf-icon.png"></a>
							</div>
						</div>
						<div class="inner-box">
							<div class="col-md-8 reset-padding">
								<h3>Triple Treat Science</h3>
								<ul class="check">
									<li>Cleanse + micro peel + LED + Skin Booster</li>
									<li>Designed by our team for those wanting more a without the fuss!</li>
								</ul>
							</div>
							<div class="col-md-4 content-price"> $149 </div>
						</div>
						

					</div>
				</div>
				<div class="clearfix visible-xs"></div>
				<div class="col-sm-4 sidebar-column">
					<?php get_sidebar('inner');?>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-12">
					<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer>
				</div>
						

			</div>
		</div>
	</div>


<?php endwhile; ?>

<script type="text/javascript">
	$(function(){
		$('.carousel').carousel({
		  interval: 3000
		})
	});
</script>


<?php get_footer(); ?>

