<?php
/*

	Template Name: Waxing Template

 */
get_header('inner'); ?>


<?php /* The loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
<div class="container-fluid">
	<div class="row">
		<div class="container">
			<img src="<?php echo bloginfo('template_directory');?>/img/inner-banner.jpg" class="img-responsive center">
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="container-fluid inner-content">
		<div class="row">
			<div class="container inner-container">
				<div class="col-sm-8 main-content-column">
					<div class="green-inner-title">
						<h2 class="page-heading"><?php the_title(); ?></h2>
					</div>
					<div class="page-section">
                                            <div class="row container-fluid">
                                                <p>We do not recycle our wax.  We utilize strip wax, while hot wax gently takes care of delicate areas
                                                making it ideal for the more sensitive skin</p>
                                            </div>
                                            <div class="col-xs-12 reset-padding">
                                                <div class="col-md-7 reset-padding">
                                                    <h3 style="margin-bottom:0">Waxing Packages available POA</h3>
                                                    please call the salon for details
                                                    <ul class="wax-list">
                                                        <li>
                                                            <div class="col-xs-6">1/2 leg</div>
                                                            <div class="col-xs-6 price">$35</div>
                                                        </li>
                                                        <li>
                                                            <div class="col-xs-6">Full leg</div>
                                                            <div class="col-xs-6 price">$50</div>
                                                        </li>
                                                        <li>
                                                            <div class="col-xs-6">Bikini x</div>
                                                            <div class="col-xs-6 price">$25</div>
                                                        </li>
                                                        <li>
                                                            <div class="col-xs-6">Bikini xx</div>
                                                            <div class="col-xs-6 price">$35</div>
                                                        </li>
                                                        <li>
                                                            <div class="col-xs-6">Bikini xxx</div>
                                                            <div class="col-xs-6 price">$59</div>
                                                        </li>
                                                        <li>
                                                            <div class="col-xs-6">U/Arms</div>
                                                            <div class="col-xs-6 price">$20</div>
                                                        </li>
                                                        <li>
                                                            <div class="col-xs-6">Arms</div>
                                                            <div class="col-xs-6 price">From $25</div>
                                                        </li>
                                                        <li>
                                                            <div class="col-xs-6">Men's Back</div>
                                                            <div class="col-xs-6 price">$45</div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-5">
                                                    <img style="border: 7px solid #eeeeee; border-radius: 160px;margin-top:10px;" src="<?php echo bloginfo('template_directory');?>/img/inner-img13.png">
                                                </div>
                                            </div>
					    <?php the_content(); ?>	

					</div>
				</div>
				<div class="clearfix visible-xs"></div>
				<div class="col-sm-4 sidebar-column">
					<?php get_sidebar('inner');?>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-12">
					<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer>
				</div>
						

			</div>
		</div>
	</div>
<?php endwhile; ?>




<?php get_footer(); ?>

