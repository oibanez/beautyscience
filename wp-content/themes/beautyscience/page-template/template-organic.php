<?php
/*

	Template Name: Organic Facials Template

 */
get_header('inner'); ?>


<?php /* The loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
<div class="container-fluid">
	<div class="row">
		<div class="container">
			<img src="<?php echo bloginfo('template_directory');?>/img/inner-banner.jpg" class="img-responsive center">
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="container-fluid inner-content">
		<div class="row">
			<div class="container inner-container">
				<div class="col-sm-8 main-content-column">
					<div class="green-inner-title">
						<h2 class="page-heading"><?php the_title(); ?></h2>
					</div>
					<div class="page-section">
						<?php the_content(); ?>	
						<div class="inner-box">
							<div class="col-md-8 reset-padding">
								<h3>Four Layer Facial</h3>
								<p>Our premier facial 'Voted best facial of the century by Comsmopolitan magazine'.</p>
								<ul class="check">
									<li>Cleanse + Exfoliation + Booster + Massage + Organic seaweed mask + Mineral thermal mask</li>
								</ul>
							</div>
							<div class="col-md-4 content-price"> $149 </div>
						</div>
						<div class="inner-box">
							<div class="col-md-8 reset-padding">
								<h3>Vita Cura Facial</h3>
								<p>Loaded with peptides, tea blends and spirulina. Provides cellular and anti-oxidant repair for dry, delicate and matured skin.</p>
								<ul class="check">
									<li>Cleanse + Enzyme peel + Booster + Massage + Spirulina organic mask</li>
								</ul>
							</div>
							<div class="col-md-4 content-price"> $159 </div>
						</div>
						<div class="inner-box">
							<div class="col-md-8 reset-padding">
								<h3>Biolight Facial</h3>
								<p>Loaded with lightening, resurfacing and anti-oxidants extract, helps brigthen and diminish the appearence of pigmentation.</p>
								<ul class="check">
									<li>Cleanse + Exfoliation peel + Booster + Massage + Brightening organic seaweed mask</li>
								</ul>
							</div>
							<div class="col-md-4 content-price"> $159 </div>
						</div>
						<div class="inner-box">
							<div class="col-md-8 reset-padding">
								<h3>Hydra Medic Science</h3>
								<p>Suited for acne, breakouts or congestion.</p>
								<ul class="check">
									<li>Cleanse + deep pore treatment + clearing booster+medicated mask + medicated light therapy.</li>
								</ul>
							</div>
							<div class="col-md-4 content-price"> $125 </div>
						</div>

						<div class="inner-box">
							<div class="col-md-7 reset-padding">
								<h3>Eye Lift Science <span class="pull-right">$39</span></h3>
								<ul class="check">
									<li>Organic sugar mask + Tea blends + Cucumber + Argireline</li>
									<li>Reduce wrinkles, puffiness and dark circles</li>
								</ul>
								<br>
								<h3>Hydra Dew Science <span class="pull-right">$79</span></h3>
								<ul class="check">
									<li>Organic seaweed mask + Argireline + Anti-oxidants + Brightening Extracts</li>
									<li>Instant hydration and plumping for a dry skin and fine lines</li>
								</ul>
							</div>
							<div class="col-md-5">
								<img src="<?php echo bloginfo('template_directory');?>/img/inner-img14.png" class="img-responsive img-circle gray-border center">
							</div>
						</div>
						<div class="inner-box">
							<div class="col-md-7 reset-padding">
								<h3>Seaweed Science <span class="pull-right">$89</span></h3>
							</div>
							<div class="col-md-12 reset-padding">
								<ul class="check">
									<li>Cleanse + Exfoliation peel + Organic seaweed mask</li>
									<li>Deep cleanses, hydrates, cools & refreshes all skin types</li>
								</ul>
							</div>

						</div>
					</div>
				</div>
				<div class="clearfix visible-xs"></div>
				<div class="col-sm-4 sidebar-column">
					<?php get_sidebar('inner');?>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-12">
					<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer>
				</div>
						

			</div>
		</div>
	</div>
<?php endwhile; ?>




<?php get_footer(); ?>

