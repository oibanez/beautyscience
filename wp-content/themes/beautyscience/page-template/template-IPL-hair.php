<?php
/*

	Template Name: IPL Hair Removal Template

 */
get_header('inner'); ?>


<?php /* The loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
<div class="container-fluid">
	<div class="row">
		<div class="container">
			<img src="<?php echo bloginfo('template_directory');?>/img/inner-banner.jpg" class="img-responsive center">
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="container-fluid inner-content">
		<div class="row">
			<div class="container inner-container">
				<div class="col-sm-8 main-content-column">
					<div class="green-inner-title">
						<h2 class="page-heading"><?php the_title(); ?></h2>
					</div>
					<div class="page-section">
                                            <div class="row container-fluid">
                                                <h3>Laser Hair Removal</h3>
                                                <p>Our operators hold a "Laser Safety Certificate". Our equipment is medical grade and TGA Approved for
                                                your peace of mind and safety for a length of time.</p>
                                            </div>
                                            <div class="col-xs-12 reset-padding">
                                                <div class="col-md-7 reset-padding">
                                                    <ul class="wax-list">
                                                        Unlimited IPL (any areas)
                                                        <li>
                                                            <div class="col-xs-6" style="padding-left:0">5 mins</div>
                                                            <div class="col-xs-6 price">$50</div>
                                                        </li>
                                                    </ul><br/><br/>
                                                    <div>
                                                        For more details on IPL Hair Removal, please see our PDF Brochure below:<br/>
                                                        <a href="<?php echo bloginfo('template_directory');?>/pdf/Spectrum_IPL_Flyer.pdf" target="_blank">
                                                            <img  src="<?php echo bloginfo('template_directory');?>/img/pdf-icon.png">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <img style="border: 7px solid #eeeeee; border-radius: 160px;margin-top:10px; width:100%" src="<?php echo bloginfo('template_directory');?>/img/inner-img11.png">
                                                </div>
                                            </div>
					    <?php the_content(); ?>	

					</div>
				</div>
				<div class="clearfix visible-xs"></div>
				<div class="col-sm-4 sidebar-column">
					<?php get_sidebar('inner');?>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-12">
					<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer>
				</div>
						

			</div>
		</div>
	</div>
<?php endwhile; ?>




<?php get_footer(); ?>

