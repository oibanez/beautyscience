<?php
/*

	Template Name: Brow and Lash Bar Template

 */
get_header('inner'); ?>


<?php /* The loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
<div class="container-fluid">
	<div class="row">
		<div class="container">
			<img src="<?php echo bloginfo('template_directory');?>/img/inner-banner.jpg" class="img-responsive center">
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="container-fluid inner-content">
		<div class="row">
			<div class="container inner-container">
				<div class="col-sm-8 main-content-column">
					<div class="green-inner-title">
						<h2 class="page-heading"><?php the_title(); ?></h2>
					</div>
					<div class="page-section">
						<?php the_content(); ?>	
						<div class="inner-box">
							<h3>Anastasia Beverley Hills </h3>
							<p>Anastasia's revolutionary eyebrow shaping system and makeup technique that achieves the PERFECT BROW is now exclusively available at Beauty Science.</p>
							<p>Our Anastasia trained Brow Artists will create the perfect brow for you utilising the latest in Hollywood techniques.</p>
							<p>Our expert stylists & colourists will create the perfect look for you and your lifestyle.</p>
							<p>'Anastasia Eyebrows' has achieved icon status among Hollywood's elite including Oprah Winfrey, Jenifer Lopez, and Madonna. </p>
							<div class="col-md-1">&nbsp;</div>
							<div class="col-md-6 reset-padding text-center">
								<img src="<?php echo bloginfo('template_directory');?>/img/inner-img1-1.png" class="img-responsive img-circle gray-border center img-brow" width="164">
							</div>
							<div class="col-md-4 content-price content-price-center"> $60 </div>
						</div>
						<div class="inner-box">
							<h3>HD Brows</h3>
							<p>Officially HD Brows trained (UK), the HD Brows unique seven-step eyebrow shaping treatment focuses on bespoke brow design. Incorporating an innovative combination of techniques including tinting, waxing, tweezing, and threading to create fabulously high definition brows.</p>
							<div class="col-md-1">&nbsp;</div>
							<div class="col-md-6 reset-padding text-center">
								<img src="<?php echo bloginfo('template_directory');?>/img/brow0.png" class="img-responsive img-circle gray-border center img-brow">
							</div>
							<div class="col-md-4 content-price content-price-center"> $70 </div>
							<div class="clearfix"></div>
							<hr>
							<h5><strong>Additional Brow Treatments</strong></h5>
							<table>
								<tr>
									<td width="250">Facial Waxing: Lip/Chin/sides</td>
									<td>$15ea</td>
								</tr>
								<tr>
									<td width="250">Facial Sugaring: Lip/Chin/Sides</td>
									<td>$20ea (highly rec)</td>
								</tr>
								<tr>
									<td width="250">Lash tint</td>
									<td>$25</td>
								</tr>
								<tr>
									<td width="250">Brow tint</td>
									<td>$20</td>
								</tr>
								<tr>
									<td width="250">Brow wax</td>
									<td>$30</td>
								</tr>
							</table>
							<p>* Package discounts apply to 2 or more areas </p>
						</div>
						<div class="inner-box">
							<h3>MYscara</h3>
							<p>Semi-permanent mascara! Myscara creates definition, volume and length. Looks fantastic on natural lashes, is formaldehyde free, hypoallergenic, smudge proof and water proof. Lasts 2-3 weeks. </p>
							<div class="col-md-1">&nbsp;</div>
							<div class="col-md-6 reset-padding text-center">
								<img src="<?php echo bloginfo('template_directory');?>/img/brow1.png" class="img-responsive img-circle gray-border center img-brow">
							</div>
							<div class="col-md-4 content-price content-price-center"> $50 </div>
						</div>
						<div class="inner-box">
							<h3>Lash Lift</h3>
							<p>This amazing technique lifts your own natural lashes giving a fanned fuller effect without the need for extensions. Unlike traditional perming, its a simpler and quicker process lasting between 4-6 weeks. </p>
							<div class="col-md-1">&nbsp;</div>
							<div class="col-md-6 reset-padding text-center">
								<img src="<?php echo bloginfo('template_directory');?>/img/brow2.png" class="img-responsive img-circle gray-border center img-brow">
							</div>
							<div class="col-md-4 content-price content-price-center"> $75 </div>
						</div>
						<div class="inner-box">
							<h3>Brow Embroidery (semi-permanent eyebrow design)</h3>
							<p>Eyebrow Embroidery is actually micro-pigmentation (cosmetic tattooing)</p>
							<p>It is a method whereby colours are inserted into the surface layer of the skin, lasting approx. 2 to 5 years.</p>

							<p>Lines that look like hairstrokes are added into the eyebrow and blended with existing hair, making it look very soft and natural, unlike previous methods that look very blocked and harsh. Treatment takes 1 hour to complete and a follow up maybe needed depending on the individual.</p>

							<p>Cosmetic Tattoo also available<br>
							Lipliner<br>
							Eyeliner<br>
							Brows </p>
							<div class="col-md-1">&nbsp;</div>
							<div class="col-md-6 reset-padding text-center">
								<img src="<?php echo bloginfo('template_directory');?>/img/brow.png" class="img-responsive img-circle gray-border center img-brow">
							</div>
							<div class="col-md-4 content-price content-price-center from"> <span>Initial fee</span><br>$595 </div>
						</div>

					</div>
				</div>
				<div class="clearfix visible-xs"></div>
				<div class="col-sm-4 sidebar-column">
					<?php get_sidebar('inner');?>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-12">
					<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer>
				</div>
						

			</div>
		</div>
	</div>
<?php endwhile; ?>

<?php get_footer(); ?>

