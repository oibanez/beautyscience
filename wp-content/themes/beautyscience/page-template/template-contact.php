<?php
/*

	Template Name: Contact Template

 */
get_header('inner'); ?>


<?php /* The loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
<div class="container-fluid">
	<div class="row">
		<div class="container">
			<img src="<?php echo bloginfo('template_directory');?>/img/inner-banner.jpg" class="img-responsive center">
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="container-fluid inner-content">
		<div class="row">
			<div class="container inner-container">
				<div class="col-sm-8 main-content-column">
					<div class="green-inner-title">
						<h2 class="page-heading"><?php the_title(); ?></h2>
					</div>
					<div class="page-section">
						<h3>Beauty Science is conveniently located at 2 great locations!</h3>
						<table class="contact-table">
							<tr>
								<td style="width: 70px;">OFFICE</td>
								<td style="width: 40px;" class="text-center">:</td>
								<td>Shop 10, 620 Victoria St, Victoria Gardens Centre Richmond, Victoria 3121</td>
							</tr>
							<tr>
								<td style="width: 70px;">PHONE</td>
								<td style="width: 40px;" class="text-center">:</td>
								<td>(03) 9427 1111</td>
							</tr>
							<tr>
								<td style="width: 70px;">EMAIL</td>
								<td style="width: 40px;" class="text-center">:</td>
								<td>richmond@beautyscience.com.au</td>
							</tr>
						</table>
						<table class="contact-table">
							<tr>
								<td style="width: 70px;">OFFICE</td>
								<td style="width: 40px;" class="text-center">:</td>
								<td>Beauty science chadstone Shop 270 1341 Dandenong road Chadstone VIC 3148</td>
							</tr>
							<tr>
								<td style="width: 70px;">PHONE</td>
								<td style="width: 40px;" class="text-center">:</td>
								<td>(03) 9569 6911</td>
							</tr>
							<tr>
								<td style="width: 70px;">EMAIL</td>
								<td style="width: 40px;" class="text-center">:</td>
								<td>chadstone@beautyscience.com.au</td>
							</tr>
						</table>

						<h3>Trading Hours for Richmond</h3>
						<table class="contact-table">
							<tr>
								<td style="width: 130px;">Mon - Tues</td>
								<td>9.00am - 6.00pm</td>
							</tr>
							<tr>
								<td style="width: 130px;">Wed - Fri</td>
								<td>9.00am - 8.00pm</td>
							</tr>
							<tr>
								<td style="width: 130px;">Sat</td>
								<td>9.00am - 5.00pm</td>
							</tr>
							<tr>
								<td style="width: 130px;">Sun</td>
								<td>by appointment only</td>
							</tr>
						</table>
						<h3>Trading Hours for Chadstone</h3>
						<table class="contact-table">
							<tr>
								<td style="width: 130px;">Mon - Tues</td>
								<td>10.00am - 5.00pm</td>
							</tr>
							<tr>
								<td style="width: 130px;">Wed - Fri</td>
								<td>10.00am - 8.00pm</td>
							</tr>
							<tr>
								<td style="width: 130px;">Sat</td>
								<td>10.00am - 5.00pm</td>
							</tr>
							<tr>
								<td style="width: 130px;">Sun</td>
								<td>by appointment only</td>
							</tr>
						</table>
						<h3>Public Holiday</h3>
						<p>We are 'closed' on these public holidays.<br>
						Christmas Day, Boxing Day, New Years Day, Australia Day, Good Friday, Easter Sunday,<br>
						Anzac day<br>
						Check store for details for public holidays not listed.</p>
						<h3>Enquiry Form</h3>
						<div>
							<?php echo do_shortcode('[contact-form-7 id="1713" title="bscontactform"]');?>
						</div>
						<br>
						<h3>Location Map</h3>
						<div class="col-sm-6">
							<p>Richmond</p>
							<iframe src="https://www.google.com/maps/embed?pb=!1m12!1m8!1m3!1d25216.494200887435!2d145.010777!3d-37.812022!3m2!1i1024!2i768!4f13.1!2m1!1sShop+10%2C+620+Victoria+St%2C+Victoria+Gardens+Centre+Richmond%2C+Victoria+3121!5e0!3m2!1sen!2sus!4v1426008342144" width="100%" height="300" frameborder="0" style="border:0"></iframe>
						</div>
						<div class="col-sm-6">
							<p>Chadstone</p>
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3148.858915138236!2d145.08309400000002!3d-37.886982!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad66a66f4050b21%3A0xffde7e9cfdd09c2!2sBeauty+Science+Chadstone!5e0!3m2!1sen!2sin!4v1426008381279" width="100%" height="300" frameborder="0" style="border:0"></iframe>
						</div>



					</div>
				</div>
				<div class="clearfix visible-xs"></div>
				<div class="col-sm-4 sidebar-column">
					<?php get_sidebar('inner');?>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-12">
					<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer>
				</div>
						

			</div>
		</div>
	</div>
<?php endwhile; ?>

<?php get_footer(); ?>

