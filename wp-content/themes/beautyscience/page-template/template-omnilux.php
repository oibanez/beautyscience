<?php
/*

	Template Name: Omnilux Template

 */
get_header('inner'); ?>


<?php /* The loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
<div class="container-fluid">
	<div class="row">
		<div class="container">
			<img src="<?php echo bloginfo('template_directory');?>/img/inner-banner.jpg" class="img-responsive center">
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="container-fluid inner-content">
		<div class="row">
			<div class="container inner-container">
				<div class="col-sm-8 main-content-column">
					<div class="green-inner-title">
						<h2 class="page-heading"><?php the_title(); ?> LED Light Therapy</h2>
					</div>
					<div class="page-section">
						<?php the_content(); ?>	
						<h3>Omnilux and Kerri Anne Kennerley</h3>
						<p>Single session $99<br>
						25% discount for Package x8 treatments (x2 per week)</p>
						<p><iframe width="100%" height="315" src="https://www.youtube.com/embed/AYqbX4yHr_A" frameborder="0" allowfullscreen></iframe></p>
						<br>
						<h3>Omnilux Light Therapy</h3>
						<p>Omnilux Light Therapy has been developed as a natural, non-invasive and relaxing procedure to help reduce the appearance of the visible signs of skin ageing minimise fine lines and wrinkles, boost collagen production and visibly improve your skin's appearance.</p>
						<p>Omnilux Light Therapy uses narrow band light emitting diodes (LEDS) to target skin cells and maximise treatment results in a wide range of dermatological conditions. FDA approved and clinically proven, Omnilux complements the body's own natural moisture renewal process to plump up the skin naturally. Also proven to accelerate wound & scar healing, it is ideal for post surgery recovery.</p>
						<p>Three different wavelengths of light (White, Blue and Red) are used in combination therapy to stimulate different cellular responses:</p>
						<h4 class="hl-lt-green">Omnilux Revive&trade;</h4>
						<p>Omnilux Revive&trade; (red light) acts to rejuvenate and repair damaged skin, improve uneven skin tone and reduce the appearance of fines line and wrinkles. Omnilux Revive delivers a precise wavelength of visible red light that penetrates deep into the skin to stimulate fibroblast activity to naturally boost the production of collagen (the protein responsible for plump, healthy skin).</p>
						<h4 class="hl-lt-green">Omnilux Blue&trade;</h4>
						<p>Omnilux Blue&trade; (blue light) is designed to deliver pure blue light at a precise wavelength. The blue light is clinically proven to be effective in the treatment of acne. By stimulating anti-bacterial and anti-inflammatory activity in the deeper layers of the skin, it works to stabilize sebum production and effectively treat active acne.</p>


						<div class="inner-box">
							<div class="col-md-12 reset-padding">
								<h3>LED Light Therapy</h3>
								<p>If you're after more rejuvenation than a normal facial but not ready for intrusive procedures, our LED treatment uses light therapy to reach areas of your skin that your therapist hands can't. It works by complimenting the body's own natural ability to refresh skin naturally and to help your skincare products work harder. See the Light, Feel the Difference!</p>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-8 reset-padding text-center">
								<br><br>
								<h3 class="text-center">Single Session</h3>
							</div>
							<div class="col-md-4 content-price"> $99 </div>
							<div class="clearfix"></div>
							<br>
							<div class="col-md-8 reset-padding text-center">
								<br><br>
								<h3 class="text-center">Additional to facial treatment</h3>
							</div>
							<div class="col-md-4 content-price"> $50 </div>
						</div>
						<p>&nbsp;</p>
						<p>For more details on Omnilux, please see our PDF Brochures below: </p>
							<div class="clearfix"></div>
							<div class="col-sm-6 text-center">
								<a href="<?php echo bloginfo('template_directory');?>/pdf/omnilux_brochure.pdf" target="_blank"><img src="<?php echo bloginfo('template_directory');?>/img/therapy-btn.png" class="img-responsive center"></a>
							</div>
							<div class="col-sm-6 text-center">
								<a href="<?php echo bloginfo('template_directory');?>/pdf/omnilux acne dl brochure 931271.pdf" target="_blank"><img src="<?php echo bloginfo('template_directory');?>/img/acne-btn.png" class="img-responsive center"></a>
							</div>

					</div>
				</div>
				<div class="clearfix visible-xs"></div>
				<div class="col-sm-4 sidebar-column">
					<?php get_sidebar('inner');?>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-12">
					<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer>
				</div>
						

			</div>
		</div>
	</div>

<?php endwhile; ?>




<?php get_footer(); ?>

