<?php
/*

	Template Name: Teeth Whitening Template

 */
get_header('inner'); ?>


<?php /* The loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
<div class="container-fluid">
	<div class="row">
		<div class="container">
			<img src="<?php echo bloginfo('template_directory');?>/img/inner-banner.jpg" class="img-responsive center">
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="container-fluid inner-content">
		<div class="row">
			<div class="container inner-container">
				<div class="col-sm-8 main-content-column">
					<div class="green-inner-title">
						<h2 class="page-heading">Professional Teeth Whitening</h2>
					</div>
					<div class="page-section">
						<?php the_content(); ?>	

						<div class="inner-box ">
                                                        <div class="col-md-8">
                                                            <h3>20 minutes</h3>
                                                            
                                                        </div>
                                                        <div class="col-md-4 content-price">
                                                            $139
                                                        </div>
						</div>

						<div class="inner-box container-fluid">
                                                        <div class="col-md-8">
                                                            <h3>30 minutes</h3>
                                                            
                                                        </div>
                                                        <div class="col-md-4 content-price">
                                                            $159
                                                        </div>
						</div>							
						<div class="inner-box container-fluid">
                                                        <div class="col-md-8">
                                                            <h3>45 minutes</h3>
                                                           
                                                        </div>
                                                        <div class="col-md-4 content-price">
                                                           $199
                                                        </div>
						</div>
                                                <div class="center-content">
                                                    <img style="border: 7px solid #eeeeee; border-radius: 160px;margin-top:10px;" src="<?php echo bloginfo('template_directory');?>/img/teeth.png">
                                                </div>

					</div>
				</div>
				<div class="clearfix visible-xs"></div>
				<div class="col-sm-4 sidebar-column">
					<?php get_sidebar('inner');?>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-12">
					<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer>
				</div>
						

			</div>
		</div>
	</div>
<?php endwhile; ?>




<?php get_footer(); ?>

