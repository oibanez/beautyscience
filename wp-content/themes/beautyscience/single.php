<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div class="main-content">
	<div class="container-fluid">
		<div class="row">
			<div class="inner-banner-container">
				<img src="<?php echo site_url();?>/wp-content/uploads/2015/02/banner-blog.jpg">

			</div>
		</div>
		<div class="row">
			<div class="container">
				<div class="page-section">
					<div class="col-sm-9">
					<?php
						// Start the Loop.
						while ( have_posts() ) : the_post();
						// twentyfourteen_posted_on();
					?>
					<h4 class="post-title"><?php the_title(); ?></h4>
					<div class="entry-meta">
						<?php
							if ( 'post' == get_post_type() )
								twentyfourteen_posted_on();?> IN
							<?php echo get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'twentyfourteen' ) );?> - 

						<?php
							if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
						?>
						<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'twentyfourteen' ), __( '1 Comment', 'twentyfourteen' ), __( '% Comments', 'twentyfourteen' ) ); ?></span>
						<?php
							endif;

							//edit_post_link( __( 'Edit', 'twentyfourteen' ), '<span class="edit-link">', '</span>' );
						?>
					</div><!-- .entry-meta -->
					<div class="feat-img-container"><?php the_post_thumbnail(); ?></div>
					<?php
							/*
							 * Include the post format-specific template for the content. If you want to
							 * use this in a child theme, then include a file called called content-___.php
							 * (where ___ is the post format) and that will be used instead.
							 */
							// get_template_part( 'content', get_post_format() );
							 the_content();
					?>
					<!-- <div class="social-media-share">
						<a href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" class="fb-share" target="_blank"><i class="fa fa-facebook"></i> Share</a>
						<a href="http://twitter.com/intent/tweet?text=<?php the_title_attribute(); ?>&url=<?php the_permalink(); ?>" class="twitter-share" target="_blank"><i class="fa fa-twitter"></i> Tweet</a>
					</div> -->
					<?php
							// Previous/next post navigation.
							// twentyfourteen_post_nav();

							// If comments are open or we have at least one comment, load up the comment template.
							 if ( comments_open() || get_comments_number() ) {
								comments_template();
							}
					?>
					<div class="continue-reading-container">
						<?php
						if(function_exists('display_social4i'))
							echo display_social4i("large","float-left");
						?>
						<!-- <a href="<?php the_permalink(); ?>" class="continue-reading">CONTINUE READING &nbsp;<i class="fa fa-angle-right"></i></a> -->
					</div>
					<?php
						endwhile;
					?>

					</div>
					<div class="col-sm-3">
						<?php get_sidebar( 'blogs' ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php
get_footer();








