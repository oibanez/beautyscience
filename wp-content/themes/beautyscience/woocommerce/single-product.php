<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



get_header('inner');
?>

<div class="container-fluid">
    <div class="row">
        <div class="container">
            <img src="<?php echo bloginfo('template_directory'); ?>/img/inner-banner.jpg" class="img-responsive center">
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="container-fluid inner-content">
    <div class="row">
        <div class="container inner-container">
            <div class="col-sm-8 main-content-column">
                <div class="green-inner-title">
                    <h2 class="page-heading">Professional Teeth Whitening</h2>
                </div>
                <div class="page-section">
                <?php
                /**
                 * woocommerce_before_main_content hook
                 *
                 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
                 * @hooked woocommerce_breadcrumb - 20
                 */
                do_action('woocommerce_before_main_content');
                ?>

                <?php while (have_posts()) : the_post(); ?>

                    <?php wc_get_template_part('content', 'single-product'); ?>

                <?php endwhile; // end of the loop.  ?>

                <?php
                /**
                 * woocommerce_after_main_content hook
                 *
                 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                 */
                do_action('woocommerce_after_main_content');
                ?>

                <?php
                /**
                 * woocommerce_sidebar hook
                 *
                 * @hooked woocommerce_get_sidebar - 10
                 */
                do_action('woocommerce_sidebar');
                ?>
                </div>
            </div>
            <div class="clearfix visible-xs"></div>
            <div class="col-sm-4 sidebar-column">
            <?php get_sidebar('inner'); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
            <?php wp_link_pages(array('before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'twentythirteen') . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>')); ?>

                <footer class="entry-meta">
                <?php edit_post_link(__('Edit', 'twentythirteen'), '<span class="edit-link">', '</span>'); ?>
                </footer>
            </div>


        </div>
    </div>
</div>





<?php get_footer(); ?>

