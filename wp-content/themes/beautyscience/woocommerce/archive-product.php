<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

get_header('inner');
?>

<div class="container-fluid">
    <div class="row">
        <div class="container">
            <img src="<?php echo bloginfo('template_directory'); ?>/img/inner-banner.jpg" class="img-responsive center">
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="container-fluid inner-content">
    <div class="row">
        <div class="container inner-container">
            <div class="col-sm-8 main-content-column">
                <div class="green-inner-title">
                    <h2 class="page-heading">Professional Teeth Whitening</h2>
                </div>
                <div class="page-section">
                    <?php
                    /**
                     * woocommerce_before_main_content hook
                     *
                     * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
                     * @hooked woocommerce_breadcrumb - 20
                     */
                    do_action('woocommerce_before_main_content');
                    ?>

                    <?php if (apply_filters('woocommerce_show_page_title', true)) : ?>

                        <h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

                    <?php endif; ?>

                    <?php do_action('woocommerce_archive_description'); ?>

                    <?php if (have_posts()) : ?>

                        <?php
                        /**
                         * woocommerce_before_shop_loop hook
                         *
                         * @hooked woocommerce_result_count - 20
                         * @hooked woocommerce_catalog_ordering - 30
                         */
                        do_action('woocommerce_before_shop_loop');
                        ?>

                        <?php woocommerce_product_loop_start(); ?>

                        <?php woocommerce_product_subcategories(); ?>

                        <?php while (have_posts()) : the_post(); ?>

                            <?php wc_get_template_part('content', 'product'); ?>

                        <?php endwhile; // end of the loop. ?>

                        <?php woocommerce_product_loop_end(); ?>

                        <?php
                        /**
                         * woocommerce_after_shop_loop hook
                         *
                         * @hooked woocommerce_pagination - 10
                         */
                        do_action('woocommerce_after_shop_loop');
                        ?>

                    <?php elseif (!woocommerce_product_subcategories(array('before' => woocommerce_product_loop_start(false), 'after' => woocommerce_product_loop_end(false)))) : ?>

                        <?php wc_get_template('loop/no-products-found.php'); ?>

                    <?php endif; ?>

                    <?php
                    /**
                     * woocommerce_after_main_content hook
                     *
                     * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                     */
                    do_action('woocommerce_after_main_content');
                    ?>

                    <?php
                    /**
                     * woocommerce_sidebar hook
                     *
                     * @hooked woocommerce_get_sidebar - 10
                     */
                   // do_action('woocommerce_sidebar');
                    ?>
                </div>
            </div>
            <div class="clearfix visible-xs"></div>
            <div class="col-sm-4 sidebar-column">
                <?php get_sidebar('inner'); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
                <?php wp_link_pages(array('before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'twentythirteen') . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>')); ?>

                <footer class="entry-meta">
                    <?php edit_post_link(__('Edit', 'twentythirteen'), '<span class="edit-link">', '</span>'); ?>
                </footer>
            </div>


        </div>
    </div>
</div>





<?php get_footer(); ?>

