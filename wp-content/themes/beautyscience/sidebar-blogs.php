<?php
/**
 * The Blogs Sidebar
 *
 **/

?>
<div class="clearfix visible-xs"></div>
<div class="search-form-container search-form-container-sidebar">
	<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
	    <div>
	        <div class="input-group">
		      <input type="text" value="" name="s" id="s" class="form-control form-control-search" placeholder="Search Blog"/>
		      <span class="input-group-btn">
		        <button class="btn btn-search" type="submit" id="searchsubmit"><i class="fa fa-search"></i></button>
		      </span>
		    </div><!-- /input-group -->
	    </div>
	</form>
</div>


<h4>Connect with Us</h4>
<ul class="sidebar-socials">
	<li><a href="#"><img src="<?php echo bloginfo('template_directory');?>/img/socials/rss.png" alt="rss"></a></li>
	<li><a href="#"><img src="<?php echo bloginfo('template_directory');?>/img/socials/twitter.png" alt="twitter"></a></li>
	<li><a href="#"><img src="<?php echo bloginfo('template_directory');?>/img/socials/facebook.png" alt="facebook"></a></li>
	<li><a href="#"><img src="<?php echo bloginfo('template_directory');?>/img/socials/ball.png" alt="ball"></a></li>
	<li><a href="#"><img src="<?php echo bloginfo('template_directory');?>/img/socials/flickr.png" alt="flickr"></a></li>
	<li><a href="#"><img src="<?php echo bloginfo('template_directory');?>/img/socials/in.png" alt="in"></a></li>
</ul>
<hr class="hr-brown">
<ul class="recent-posts-list">
	<?php 
	$args = array( 'post_type' => 'post', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 5, 'category_name' => 'blog' );
	$wp_query = new WP_Query($args);
	while ( have_posts() ) : the_post(); ?>
<!-- --><li>
   			
			<!-- <a href="<?php the_permalink(); ?>" class="permalink"> -->
				
				<!-- <div class="entry-meta">
					<span class="cat-links">Category: <?php echo get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'twentyfourteen' ) ); ?></span>
				</div>  -->
		
			<!-- </a> -->
					
						<div class="recent-feat-img-container"><a href="<?php the_permalink(); ?>" class="permalink"><?php the_post_thumbnail('thumbnail'); ?></a></div>
						<div class="recent-desc">
							<a href="<?php the_permalink(); ?>" class="permalink"><h4 class="recent-post-title"><?php the_title(); ?></h4></a>
							<div class="entry-meta">
								<?php
									if ( 'post' == get_post_type() )
										twentyfourteen_posted_on();
									


									// if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
								?>
								<!-- <span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'twentyfourteen' ), __( '1 Comment', 'twentyfourteen' ), __( '% Comments', 'twentyfourteen' ) ); ?></span> -->
								<?php
									// endif;

									//edit_post_link( __( 'Edit', 'twentyfourteen' ), '<span class="edit-link">', '</span>' );
								?>
							</div><!-- .entry-meta -->
						</div>


					
					
   		</li><!-- -->
	<?php endwhile; ?>
</ul>
<div class="clearfix"></div>
<hr class="hr-brown">
<div class="tags-container">
<h4>Tags</h4>
	<?php the_tags('',' '); ?>
</div>