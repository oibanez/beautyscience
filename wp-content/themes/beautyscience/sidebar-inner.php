<?php
/**
 * The Inner Sidebar
 *
 **/

?>
<div class="green-inner-title">
	<div class="header-contact-details">
        <img src="<?php echo bloginfo('template_directory');?>/img/phone-icon-big.png" alt="phone" class="phone-icon">
        <p class="phone">03 9427 1111 - Richmond <br>
            03 9569 6911 - Chadstone</p>
    </div>
</div>
<div class="page-section">
	<h3>Facial Treatments</h3>
	<ul class="sidebar-nav">
	    <li><a href="<?php echo get_permalink('1491');?>">Organic Facials</a></li>
	    <li><a href="<?php echo get_permalink('1494');?>">Facial Rejuvenation</a></li>
	    <li><a href="<?php echo get_permalink('1496');?>">Omnilux</a></li>
	</ul>
	<h3>Hair Removal</h3>
	<ul class="sidebar-nav">
	    <li><a href="<?php echo get_permalink('1512');?>">Sugaring</a></li>
	    <li><a href="<?php echo get_permalink('1510');?>">Waxing</a></li>
	    <li><a href="<?php echo get_permalink('1514');?>">IPL Hair Removal</a></li>
	</ul>
	<h3>Body Treatments</h3>
	<ul class="sidebar-nav">
	    <li><a href="<?php echo get_permalink('1504');?>">Cellulite</a></li>
	    <li><a href="<?php echo get_permalink('1506');?>">Teeth whitening</a></li>
	    <li><a href="<?php echo get_permalink('1508');?>">Spray Tan</a></li>
	</ul>

	<ul class="sidebar-callouts">
	    <li><a href="#"><img src="<?php echo bloginfo('template_directory');?>/img/sideimg1.jpg" alt="" class="center-block img-responsive"></a></li>
	    <li><a href="<?php echo get_permalink('1504');?>"><img src="<?php echo bloginfo('template_directory');?>/img/sideimg2.jpg" alt="" class="center-block img-responsive"></a></li>
	    <li><a href="<?php echo get_permalink('1709');?>"><img src="<?php echo bloginfo('template_directory');?>/img/sideimg3.jpg" alt="" class="center-block img-responsive"></a></li>
	    <li><a href="<?php echo get_permalink('1512');?>"><img src="<?php echo bloginfo('template_directory');?>/img/sideimg4.jpg" alt="" class="center-block img-responsive"></a></li>
	</ul>



</div>