<div class="search-form-container">
	<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
	    <div>
	        <div class="input-group">
		      <input type="text" value="" name="s" id="s" class="form-control" />
		      <span class="input-group-btn">
		        <button class="btn" type="submit" id="searchsubmit"><i class="fa fa-search"></i></button>
		      </span>
		    </div><!-- /input-group -->
	    </div>
	</form>
</div>
