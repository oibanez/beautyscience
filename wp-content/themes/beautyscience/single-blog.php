<?php
/**
 * The Template for displaying all single blog posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div class="main-content">
	<div class="container-fluid">
		<div class="row">
			<div class="container">
				<div class="page-section">
					<div class="col-sm-8">
					<?php
						// Start the Loop.
						while ( have_posts() ) : the_post();
						// twentyfourteen_posted_on();
					?>
					<h4 class="post-title"><?php the_title(); ?></h4>
					<?php
							/*
							 * Include the post format-specific template for the content. If you want to
							 * use this in a child theme, then include a file called called content-___.php
							 * (where ___ is the post format) and that will be used instead.
							 */
							// get_template_part( 'content', get_post_format() );
							 the_content();
					?>
					<!-- <div class="social-media-share">
						<a href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" class="fb-share" target="_blank"><i class="fa fa-facebook"></i> Share</a>
						<a href="http://twitter.com/intent/tweet?text=<?php the_title_attribute(); ?>&url=<?php the_permalink(); ?>" class="twitter-share" target="_blank"><i class="fa fa-twitter"></i> Tweet</a>
					</div> -->
					<?php
							// Previous/next post navigation.
							// twentyfourteen_post_nav();

							// If comments are open or we have at least one comment, load up the comment template.
							/* if ( comments_open() || get_comments_number() ) {
								comments_template();
							}*/
					?>
					<?php
						endwhile;
					?>

					</div>
					<div class="col-sm-4">
						<?php // get_sidebar( 'content' ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php
get_footer();








