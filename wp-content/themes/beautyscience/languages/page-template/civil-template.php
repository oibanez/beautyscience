<?php
/*

	Template Name: Civil Template

 */
get_header('civil');
?>


<div class="container-fluid">
    <div class="row">
        <div class="container">
            <div class="page-section">
                <?php /* The loop */ ?>
                <?php while (have_posts()) : the_post(); ?>
                    <div class="clearfix"></div>
                    <div class="col-sm-12 about-container">
                            <!--<h3 class="page-heading"><?php the_title(); ?></h3>-->
                        <div class="col-sm-6" style="padding:0px;margin:0px">
                            <img src="<?php the_field('page_picture') ?>" class="col-sm-12" style="padding:0px;">
                        </div>
                        <div class="col-sm-6" style="padding-left:40px; padding-right:45px">
                            <div style="padding-top:20px">
                                <h2 class="font-blue"><?php the_field('title') ?></h2>
                            </div>
                            <?php the_content(); ?>
                        </div>
                            <?php wp_link_pages(array('before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'twentythirteen') . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>')); ?>
                        
                        <div class="clearfix"></div>
                        <footer class="entry-meta">
                            <?php edit_post_link(__('Edit', 'twentythirteen'), '<span class="edit-link">', '</span>'); ?>
                        </footer>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</div>





<?php get_footer(); ?>

