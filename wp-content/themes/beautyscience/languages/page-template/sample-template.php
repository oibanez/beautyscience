<?php
/*

	Template Name: Sample Template

 */
get_header(); ?>


<div class="container-fluid">
		<div class="row">
			<div class="container">
				<div class="page-section">
					<?php /* The loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>
						<div class="col-sm-12" style="min-height: 500px;">
							<h3 class="page-heading"><?php the_title(); ?></h3>
							<?php the_content(); ?>
							<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
							<div class="clearfix"></div>
							<footer class="entry-meta">
								<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
							</footer>
						</div>
						<?php endwhile; ?>
				</div>
			</div>
		</div>
	</div>





<?php get_footer(); ?>

