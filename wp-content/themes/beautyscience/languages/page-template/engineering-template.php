<?php
/*

  Template Name: Engineering Template

 */
get_header('engineering');
?>


<div class="container-fluid">
    <div class="row">
        <div class="container">
            <div class="page-section">
                <?php /* The loop */ ?>
                <?php while (have_posts()) : the_post(); ?>
                    <div class="clearfix"></div>
                    <div class="col-sm-12" style="min-height: 935px">
                        <div class="col-sm-12 engineering-content">
                            <div class="col-sm-4" style="min-height:380px; padding-left:10px ">
                                <img src="<?php the_field('article1_image') ?>" class="col-sm-12" style="padding:0px">
                                <div class="col-sm-12 dark-background" style="min-height: 385px; padding-bottom:25px">
                                    <?php the_field('article1_content') ?>
                                </div>
                            </div>
                            <div class="col-sm-8" style="min-height: 350px; padding-left: 5px; padding-bottom:25px">
                                <img src="<?php the_field('article2_image') ?>" class="col-sm-6" style="padding-right: 0px; padding-left: 15px; padding-top: 0; padding-bottom: 0">
                                <div class="col-sm-6 blue-background" style="min-height: 349px">
                                    <div class="col-sm-12">
                                        <?php the_field('article2_content') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4" style="padding-right:5px;">
                                 <img src="<?php the_field('middle_image') ?>" class="col-sm-12" style="padding:0 0 0 5px;">
                            </div>
                            <div class="col-sm-4" style="min-height:380px; padding-left:10px ">
                                
                                <div class="col-sm-12 dark-background">
                                    <?php the_field('article3_content') ?>
                                </div>
                            </div>
                            <div class="col-sm-8" style="min-height: 350px; padding-left: 5px; padding-bottom:25px">
                                <div class="col-sm-12 blue-background" style="min-height: 349px">
                                    <div class="col-sm-12">
                                        <?php the_field('article4_content') ?>
                                    </div>
                                </div>
                            </div>
                        </div
                        
                        <?php wp_link_pages(array('before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'twentythirteen') . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>')); ?>

                        <div class="clearfix"></div>
                        <footer class="entry-meta">
                            <?php edit_post_link(__('Edit', 'twentythirteen'), '<span class="edit-link">', '</span>'); ?>
                        </footer>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</div>





<?php get_footer(); ?>

