<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
    <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width">
        
        <META name="keywords" content="" />	
        <link rel="icon" type="image/png" href="<?php echo bloginfo('template_directory'); ?>/img/favicon.ico">
        <title>Beauty Science</title>
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <link href="<?php echo bloginfo('template_directory'); ?>/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo bloginfo('template_directory'); ?>/css/font-awesome.min.css" rel="stylesheet" media="screen">	
        <link href="<?php echo bloginfo('template_directory'); ?>/fonts/font-face.css" rel="stylesheet" media="screen" >
        <link href="<?php echo bloginfo('template_directory'); ?>/css/main.css" rel="stylesheet" media="screen">
        <link href="<?php echo bloginfo('template_directory'); ?>/css/style.css" rel="stylesheet" media="screen">
        <link href="<?php echo bloginfo('template_directory'); ?>/css/desktop.css" rel="stylesheet" media="screen">
        <link href="<?php echo bloginfo('template_directory'); ?>/css/tablet.css" rel="stylesheet" media="screen">	
        <link href="<?php echo bloginfo('template_directory'); ?>/css/phone.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>

        <script type="text/javascript" language="javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
        <script src="<?php echo bloginfo('template_directory'); ?>/js/modernizr.custom.js"></script>
        <!-- script type="text/javascript" language="javascript" src="<?php //echo bloginfo('template_directory');  ?>/js/jquery.slimscroll.min.js"></script -->
        <script src="<?php echo bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>

        <!--[if IE]>
                <link href="<?php echo bloginfo('template_directory'); ?>/css/all-ie.css" rel="stylesheet" media="screen">
        <![endif]-->
        <!--[if lt IE 9]>
                <script src="<?php echo bloginfo('template_directory'); ?>/js/html5.js"></script>
        <![endif]-->

        <?php wp_head(); ?>

        <style type="text/css">
            li.menu-item-1537 {
                float: right !important;
                margin-right: 10px;
            }
            li#menu-item-1536 {
                display: none;
            }
            @media (max-width: 991px) {
                li.menu-item-1537 {
                    float: none !important;
                    margin-right: 0px;
                }
            }
            @media (min-width: 1370px) {
                .container {
                  width: 1170px;
                }
            }
        </style>
    </head>

    <body <?php body_class(); ?>>

<!-- ################ 
        HEADER 
##################-->

<div class="container-fluid">
    <div class="row">
        <header class="header-container">
            <div class="container">
                    <div class="col-md-3">
                        <h1 class="text-center logo-container"><img src="<?php echo bloginfo('template_directory'); ?>/img/beautyscience.png" class="img-responsive inner-logo" alt="Beauty Science"></h1>
                    </div>

                    <!-- FOR MOBILE -->
                    
                    <div class="col-md-3 reset-padding visible-xs">
                        <div class="header-contact-details">
                            <a href="https://www.facebook.com/pages/Beauty-Science/106853469334990?v=wall" target="_blank" class="fb-link">
                                <img src="<?php echo bloginfo('template_directory');?>/img/fb-icon.png" alt="Beaty Science Facebook Page" class="fb-icon">
                            </a>
                            <img src="<?php echo bloginfo('template_directory');?>/img/phone-icon.png" alt="phone" class="phone-icon">
                            <p class="phone">03 9427 1111 - Richmond <br>
                                03 9569 6911 - Chadstone</p>
                        </div>
                        <p class="visible-xs text-center"><a href="#" class="shop-now">SHOP NOW</a></p>
                    </div>

                    <div class="col-md-7">
                        <nav class="navbar navbar-inverse" role="navigation">
                         <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-main">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                              </button>
                                <!-- <a class="navbar-brand" href="#">Menu</a> -->
                            </div>
                            <?php
                                $defaults = array(
                                    'theme_location' => '',
                                    'menu' => '',
                                    'container' => 'div',
                                    'container_class' => 'collapse navbar-collapse',
                                    'container_id' => 'navbar-collapse-main',
                                    'menu_class' => 'menu',
                                    'menu_id' => '',
                                    'echo' => true,
                                    'fallback_cb' => 'wp_page_menu',
                                    'before' => '',
                                    'after' => '',
                                    'link_before' => '',
                                    'link_after' => '',
                                    'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                    'depth' => 0,
                                    'walker' => new BootstrapNavMenuWalker()
                                );
                                wp_nav_menu($defaults);
                            ?>
                        </nav>
                    </div>
                    <div class="col-md-2 reset-padding hidden-xs inner-fb-container">
                        <a href="https://www.facebook.com/pages/Beauty-Science/106853469334990?v=wall" target="_blank" >
                            <img src="<?php echo bloginfo('template_directory');?>/img/fb-icon.png" alt="Beaty Science Facebook Page" class="fb-icon">
                        </a>
                    </div>
            </div><!-- /.container -->
        </header><!-- /.header-container -->
    </div><!-- /.row -->
</div><!-- /.container-fluid -->