<div class="container-fluid footer-cont-rp reset-padding">
	<div class="row reset-margin">
		<div class="container ">
			<footer class="footer-container">
			<br>
				<?php echo get_template_part('footer-callouts');?>
			</footer>
		</div>
	</div>
	<div class="row reset-margin">
		<p class="copyright">&copy; Beauty Science <?php echo date('Y');?> All Rights Reserved.<br>
			<a href="http://spafresh.com.au">A Spa Fresh Design</a>
		</p>
	</div>
</div>

    <script src="<?php echo bloginfo('template_directory'); ?>/js/respond.js"></script>
	<script src="<?php echo bloginfo('template_directory'); ?>/js/main.js"></script>
	<?php wp_footer(); ?>
</body>
</html>