<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

	<div class="container-fluid">
		<div class="row">
			<div class="container">
				<div class="page-section">
					<div class="col-sm-12">
						<div class="col-sm-12">
							<h3 class="page-heading"><?php the_title(); ?></h3>
						</div>
						<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentyfourteen' ); ?></p>

				<?php get_search_form(); ?>
						
					</div>
				</div>
			</div> <!-- /.container -->
		</div> <!-- /.row -->
	</div> <!-- /.container-fluid -->
	



<?php
// get_sidebar( 'content' );
// get_sidebar();
get_footer();
