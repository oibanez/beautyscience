<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'beautyscience_wpdb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'i>|y$Eo,s(xY7#j}5s!J-QImgHQ&3d%t|K>is+~8rv(78odw# 6%CM~0-#KrC3Km');
define('SECURE_AUTH_KEY',  'KnzSgt `=|%4U]e.o3wm+aT[^/(xS`g<&/f@G|z>n;VVKVb1W:UNjaYftvL$2JB+');
define('LOGGED_IN_KEY',    't!zh3{Atg>5s9HBPll0SEn<h)P:Ks{r$pkF#q!0;]n^+g(/CZ#O[X IvDqjNEM{}');
define('NONCE_KEY',        '<r:;[z>E-*WlgbbgNv.Kx(lo&_f9*i^=,E<l&`ie_U2LJPF`ubXFgM;]+h# tAvz');
define('AUTH_SALT',        '3r0^5>W!i#Bfu@Nkorc 9&L+!&Q<Y$tl+!9Yagkf5c-cU/INJ#dS-Eu97uTVaFzB');
define('SECURE_AUTH_SALT', '`->IfdoCO$D~|pK9f8L>@-!$y+h`>~~-+OQJ[[I~}Y1~3q}M+jGbpl$)> /-ZT,F');
define('LOGGED_IN_SALT',   'Re]|]:Yi,SJ:~O+?D`5|sab|`<z1qy`+eh[FpuD|RneTL9scT&pK6{3f)i<^$%kH');
define('NONCE_SALT',       'uXy,]bYmo*l&E>~9<1L<n5$+pBcw.[3=T *(6^=v4cS{hF|QkM.+%%0eA-2IVgp*');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
